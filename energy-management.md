# Energy Management
___
### 1. What are the activities you do that make you relax - Calm quadrant?
- Listening to my favorite music.
- Spending time in nature, such as walking in a park or by the beach.
- Drinking a warm water or tea.
- Making a cozy environment with dim lighting to relax my body and sleep.

### 2. When do you find getting into the Stress quadrant?
- While learning new things.
- When there is a delay in work.
- When there are tight deadlines.
- When I am in unpleasant environments.

### 3. How do you understand if you are in the Excitement quadrant?
- Successfully overcoming a challenging task or achieving a goal.
- Receiving acknowledgment or recognition for my efforts.
- Spending time with loved ones.
- Achieving personal development goals.

### 4. Paraphrase the Sleep is your Superpower [video](https://www.youtube.com/watch?v=5MuIMqhT8DM) in your own words in brief. Only the points, no explanation.
- Lack of sleep has significant effects on reproductive health.
- Sleep is crucial for learning and memory.
- Sleep deprivation leads to a 40% deficit in the brain's ability to form new memories.
- Sleep disruption is linked to aging, cognitive decline, and Alzheimer's disease.
- Sleep loss affects the cardiovascular system.
- Sleep deficiency weakens the immune system.
- Short sleep duration is associated with a higher risk of cancer, including breast, prostate, and bowel cancer.
- Lack of sleep negatively impacts gene activity, affecting immune system genes and promoting tumor-related genes.

### 5. What are some ideas that you can implement to sleep better?
- Going to bed and waking up at the same time every day, even on weekends.
- Making cozy environment to get better sleep.
- Reducing screen time before going to bed.
- Avoid heavy meals and caffeine.

### 6. Paraphrase the [video](https://www.youtube.com/watch?v=BHY0FxzoKZE) - Brain Changing Benefits of Exercise. Minimum 5 points, only the points.
- Wendy Suzuki starts by talking about how physical activity can have a long-lasting positive impact on one's well-being.
- She tells about the brain structure; the front part is called the prefrontal cortex, which is responsible for decision-making.
- We also have two temporal lobes on each side of our brain. This part of the brain is responsible for remembering events and memories.
- Permanent memories are stored in Hippocampus.
- Exercise immediately increases neurotransmitters like dopamine, serotonin, and noradrenaline, leading to improved mood and attention for at least two hours.
- Exercise acts as a protective shield for the brain, creating a stronger hippocampus and prefrontal cortex. 
- This resilience delays the onset of neurodegenerative diseases and mitigates normal cognitive decline associated with aging.
- To get these benefits, a moderate approach suffices. Aim for three to four sessions of aerobic exercise per week, lasting at least 30 minutes each. 
- Simple activities like walking, taking stairs, or even vigorous housecleaning can contribute to overall brain health.

### 7. What are some steps you can take to exercise more?
- Begin with short, manageable sessions, like a 10-minute walk or a quick home workout.
- Adding an extra walk around the block, taking stairs.
- By trying various activities to prevent boredom and worked different muscle groups.
- Consistency.