# SOLID: The First 5 Principles of Object Oriented Design

## Overview
A collection of Object-Oriented design principles known as the SOLID Principles are a collection of guidelines and best practices that have entirely changed how software is written. Any software system should be flexible to adapt to changes, and any new features added should be simple to understand and use. These principles improve our understanding of organizing code, making it flexible, robust, manageable, and easy to test.

## Introduction

[Robert C. Martin(Uncle Bob)](https://en.wikipedia.org/wiki/Robert_C._Martin) presented the SOLID design principles in his 2000 paper ["Design Principles and Design Patterns"](https://staff.cs.utu.fi/~jounsmed/doos_06/material/DesignPrinciplesAndPatterns.pdf). But Michael Feathers came up with the acronym later. These principles are:


- <span style="color:red">S</span>  - [**Single-responsibility Principle**](https://gitlab.com/bjbabu/life-skills-track/-/blob/main/technical-paper.md?ref_type=heads#single-responsibility-principle-srp)
- <span style="color:red">O</span>  - [**Open-closed Principle**](https://gitlab.com/bjbabu/life-skills-track/-/blob/main/technical-paper.md?ref_type=heads#open-closed-principle-ocp)
- <span style="color:red">L</span>  - [**Liskov Substitution Principle**](https://gitlab.com/bjbabu/life-skills-track/-/blob/main/technical-paper.md?ref_type=heads#liskov-substitution-principle)
- <span style="color:red">I</span>  - [**Interface Segregation Principle**](https://gitlab.com/bjbabu/life-skills-track/-/blob/main/technical-paper.md?ref_type=heads#interface-segregation-principle)
- <span style="color:red">D</span>  - [**Dependency Inversion Principle**](https://gitlab.com/bjbabu/life-skills-track/-/blob/main/technical-paper.md?ref_type=heads#dependency-inversion-principle)


This article introduces each principle individually using JavaScript as the sample programming language.

## Single-Responsibility Principle (SRP)

To put it simply, that a class, module, or function should only have one job.
When a class has logic to do several tasks, it will become more difficult to debug and manage as the codebase grows larger. It will be challenging for new developers to attempt to read and comprehend the code. Because each key operation is handled by a single class, separating the logic and making each class do a single task will make the code easier to maintain and debug as well as reusable.

For example, we created a function for validating a user and inserting details into database.

- Here, two tasks are executing in single function. 
```js
//Wrong way according to SRP.
validateAndCreateUser = (name, password, email) => {
    //Validating the user.
    const isFormValid = testForm(name, password, email);
    //Inserting data into database.
    if(isFormValid) {
        User.Create(name, password, email)
    }
}

```
- So, according to SRP, we need to divide above two tasks into two separate functions like below.

```js
//Correct way according to SRP.
//It only validates the user.
validateequest = (req) => {
    const isFormValid = testForm(name, password, email);
    //If form is valid, it calls create user function.
    if(isFormValid){
        createUser(req);
    }
}
//It only inserts data into database.
createUser = (req) => User.Create(req.name, req.password, req.emial);
```

## Open-Closed Principle (OCP)

The OCP states that objects or entities(classes, modules, functions ...etc.) should be open for extension but closed for modification. This means that implementing these entities should allow for the extension of their functionality to other entities without requiring changes to the original entity's code. Bertrand Meyer originated the term OCP.

- For example, look at the below code. There is an array containing roles, and there is a function for checking whether the role is present or not.

```js
const roles = ["ADMIN", "USER"];

checkRole = (role) => {
    if(roles.includes(role)){
        return true;
    }else{
        return false;
    }
}

//Test role.
checkRole("Admin"); //true
checkRole("Manager"); //false
```

- If, in the future, we want to add other roles, the function should allow us to add them without modifying the existing code. 

```js
const roles = ["ADMIN", "USER"];

checkRole = (role) => {
    if(roles.includes(role)){
        return true;
    }else{
        return false;
    }
}

//Function for adding new role
addRole = (role) => {
    roles.push(role);
}

//Adding role
addRole("Manager");  //["ADMIN", "USER", "Manager"]


//Test role.
checkRole("Admin"); //true
checkRole("Manager"); //true
```

## Liskov Substitution Principle

According to the Liskov Substitution Principle, all subclasses and derived classes must be able to be substituted for their base or parent classes.

'S' is a subtype of T, then objects of type 'T' may be replaced with objects of type 'S'. It is introduced by Barbara Liskov.
- Let's look at the example below.

```js
//Wrong way according to LSP.
//Base class
class Bird{  
    fly(){
        //
    }
}

//Eagle extending the Bird class.
class Eagle extends Bird {
    dive(){
        //
    }
}

//Eagle can use both functions.
const eagle = new Eagle();
eagle.fly();
eagle.dive();

//Here, Penguin extends the Bird class but Penguin can't fly.
class Penguin extends Bird(){
    //Problem : Can't fly!!!
}
```

- The base class has a method, so we should not extend this base class to any of the other methods that are not related. So, in the above code, the penguin cannot extend or write a function to the 'fly()' method.
  
```js
//Correct way according to LSP.
//Base class
class Bird{
    layEgg(){}
}

//Flying Bird extends the Bird class.
class FlyinBird extends Bird{
    fly(){}
}

//Swimming Bird extends the Bird class.
class SwimmingBird extends Bird{
    swim(){}
}

class Eagle extends FlyingBird{};

class Penguin extends SwimmingBird{};

const penguin = new Penguin();
penguin.swim();
penguin.layEgg();

const eagle = new Eagle();
eagle.fly();
eagle.layEgg()
```

- Specified the bird and established its characteristics by creating extra classes. We may get 'n' number of classes, but those will be the only reasonable methods to be implemented.

## Interface Segregation Principle

The Interface Segregation Principle states that larger interfaces must be split into smaller ones. By doing this, we can ensure that implementing classes is only focused on the methods that are relevant to clients. Having several client-specific interfaces is better than having one generic interface.

Clients should never be made to depend on methods or interfaces they do not use, nor should they be forced to implement them.

- In the below example, we create user class, where it creates user and validates.
  
```js
//Wrong way according to ISP.
class User {
    constructor(username, password) {
        this.username = username;
        this.password = password;
        this.initiateUser();
    }

    initiateUser() {
        this.validateUser();
    }

    validateUser() {
        console.log("Validating...");
    }
}

const user = new User("JB", "123456");
console.log(user);

//Validating...
//User { username: 'JB', password: '123456' }

```

- In the above code, creating a user will automatically invoke the initiateUser and validateUser functions. It may only be required for some users, but it will do for all users.
- To avoid this, we can add a flag while creating a user as shown below.

```js
//Correct way according to ISP.
class User {
    constructor(username, password, validate) {
        this.username = username;
        this.password = password;
        this.validate = validate;
        if(validate){ //Check whether the user has to be initiated and validated.
            this.initiateUser();
        }else{
            console.log("No validation required");
        }
    }

    initiateUser() {
        this.validateUser();
    }

    validateUser() {
        console.log("Validating...");
    }
}

const user = new User("JB", "123456", true);
console.log(user);

//Validating...
//User { username: 'JB', password: '123456', true }
```

- In the above code, it only validates for specific users.

## Dependency Inversion Principle

The Dependency Inversion Principle states that high-level modules should not depend on low-level modules, both should depend on abstractions, and abstractions should not depend on details. Suppose a high-level entity depends on the low-level entity. In that case, the code will have a tight coupling, and changing one entity can break another, which is problematic at the production level. So, here, the fundamental goal is to decouple dependencies.

- Below is an example of a `PasswordReminder` that conncets to a MySQL DB.
  
```js
class MySQLConnection {
    connect() {
        // handle the database connection
        return 'Database connection';
    }
}

class PasswordReminder {
    constructor() {
        this.dbConnection = new MySQLConnection();
    }
}

const passwordReminder = new PasswordReminder();
console.log(passwordReminder.dbConnection.connect());
```

- The above code violates the DIP rule because `PasswordReminder` depends on `MySQLConnection`. If we change the DB engine, we must edit `passwordReminder`.

```js
// Define an interface-like structure
class DBConnectionInterface {
    connect() {
        throw new Error('Method connect() must be implemented');
    }
}

// Implement the interface in the MySQLConnection class
class MySQLConnection extends DBConnectionInterface {
    connect() {
        // handle the database connection
        return 'Database connection';
    }
}

// PasswordReminder class taking a DBConnectionInterface instance in the constructor
class PasswordReminder {
    constructor(dbConnection) {
        this.dbConnection = dbConnection;
    }
}

const mySQLConnection = new MySQLConnection();
const passwordReminder = new PasswordReminder(mySQLConnection);

console.log(passwordReminder.dbConnection.connect());
```

- The `MySQLConnection` class implements the interface with a connect method. So, `passwordReminder` hints at the `DBConnectionInterface`, which implies that no matter what type of DB you use, the `passwordReminder` will connect without problem.


## Conclusion

The main goal of the SOLID design principles is to remove dependencies so that programmers can change one program entity without affecting others. Additionally, they should make designs simpler to understand, maintain, and extend. Ultimately, software engineers can use these design principles to prevent issues and produce adaptable, agile, and maintainable software.

## References

- [SOLID: The First Five Principles of Object-Oriented Design](https://www.digitalocean.com/community/conceptual_articles/s-o-l-i-d-the-first-five-principles-of-object-oriented-design)

- [SOLID Principles in JavaScript](https://www.xenonstack.com/blog/solid-principles-javascript)

- [YouTube Playlist: SOLID Principles in Object-Oriented Design](https://www.youtube.com/playlist?list=PL6n9fhu94yhXjG1w2blMXUzyDrZ_eyOme)

- [YouTube Video: SOLID Principles in Object-Oriented Design](https://www.youtube.com/watch?v=m2GCb-x8e5s)


