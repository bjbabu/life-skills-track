# Learning Process

### 1. What is the Feynman Technique? Explain in 1 line.
If you want to **understand** something well, **explain it.**

### 2. In this video, what was the most interesting story or idea for you?
The speaker's story, Barbara Oakley, who initially struggled with Math and Science and later transformed into a Professor of Engineering, and the stories of Salvador Dali and Thomas Edison, who used relaxation techniques to transition from the diffuse mode to focus mode, from that how she addresses the Pomodoro idea to enhance focused attention caught my interest.

### 3. What are active and diffused modes of thinking?
**Active mode:**
- It is called focused mode.
- Channelling focus on a specific thought.
- Beneficial for works that require careful and precise thinking.

**Diffuse mode:**
- Relaxed set of neural states.
- Wide range of thoughts involved and loosely connected.
- Beneficial for tasks that require different perspectives.

### 4. According to the video, what are the steps to take when approaching a new topic? Only mention the points.

- Deconstruct the skill.
- Learn enough to self-correct or self-edit.
- Remove barriers to practice.
- Practice for at least 20 hours.

### 5. What are some of the actions you can take going forward to improve your learning process?
- **Deconstruct the skill:**
  - Deciding what I want to achieve.
  - Breaking down the skill into smaller, manageable pieces.
  - Prioritize aspects according to their importance.
<br>

- **Learn enough to self-correct or self-edit:**

  - Gathering three to five resources about the skill you're learning.
  - Learning enough to be able to practice and self-correct as you go.
  <br>

- **Remove barriers to practice:**

  - Identifying and eliminating distractions (e.g., TV, internet) during practice.
  - Using willpower to create a focused and distraction-free environment.
<br>

- **Practice for at Least 20 Hours:**

  - Committing to practice for at least 20 hours to overcome the initial barrier of frustration.
  - Understanding that the early stages may be challenging, but continued practice can reach the goal.
<br>

- **Enjoy the process:**
  - Enjoying the journey of acquiring a new skill rather than focusing solely on the result.
  - Celebrating small victories along the way.