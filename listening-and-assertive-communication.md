# Listening and Active Communication
___
### 1. What are the steps/strategies to do Active Listening?
- Give full attention to the speaker, make eye contact, and eliminate distractions.
- Expressing our interest by physical gestures.
- Allowing the speaker to finish without intersecting your thoughts in the middle.
- Speaking phrases that show our interest.
- Summarize the speaker's words in your voice to confirm understanding.
- Providing thoughtful responses that address the speaker's points.
- Take notes to capture essential information and show its importance.

### 2. According to Fisher's model, what are the key points of Reflective Listening?
- **Empathy:** Understand the speaker's perspective without judgment, expressing it verbally and nonverbally.
- **Acceptance:** Respect the person unconditionally, avoiding agreement or disagreement.
- **Congruence:** Be open and genuine, acknowledging and communicating your feelings.
- **Concreteness:** Focus on specifics rather than vague generalities, encourage clarity by asking for specifics.

### 3. What are the obstacles in your listening process?
- I interrupt the speaker by keeping my ideas and thoughts in the middle.

### 4. What can you do to improve your listening?
- Hold my ideas and thoughts, allowing the speaker to finish, later, I will share my views and opinions.

### 5. When do you switch to Passive communication style in your day to day life?
- When I want to avoid confrontation or conflict.

### 6. When do you switch into Aggressive communication styles in your day to day life?
- Most of the time, I won't switch to aggressive communication.

### 7. When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?
- I switch to passive-aggressive communication when I am with friends or relations.

### 8. How can you make your communication assertive? You can watch and analyse the videos, then think what would be a few steps you can apply in your own life?
- Focus on expressing my actual feelings rather than judgments or evaluations of others' behavior.
- Understanding the underlying needs driving my requests. This clarity helps in effective communication.
- Practicing assertive communication in easier situations with safe people to build confidence.
- Focusing on my body language and tone of voice to be assertive in nature.
- Addressing problematic situations as early as possible. 
- Communicate your needs and boundaries clearly.