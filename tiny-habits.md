# Tiny Habits
___
### 1. In this [video](https://www.youtube.com/watch?v=AdKUJxjn-R8), what was the most interesting story or idea for you?
- Most of the behaviors that we need to do are habits. You can change your life forever when you know how to create tiny habits. The formula to hone the tiny habits is that these three things have to happen simultaneously to cause behavior: motivation, ability, and trigger (B = MAT). The format for tiny habit is that after doing an existing behavior, you have to practice it so that it will be automatized. After completing a tiny habit, celebrate yourself in your own way so you will get motivated.

### 2. How can you use B = MAP to make making new habits easier? What are M, A and P.
- B = MAP , it is the formula introduced by BJ Fogg. 
  - **M** = Motivation.
  - **A** = Ability.
  - **P** = Prompt.
- To make new habits easier, we can use B = MAP as follows:
  - Shrinking the behavior to its tiniest version, making it easy to do in 30 seconds or less. It minimizes the need for high motivation.
  - Choosing an action prompt that follows the completion of an existing behavior. Action prompts leverage the momentum from one behavior to initiate another.
  - Celebrating the completion of the tiny habit to generate a positive feeling. It helps build confidence, motivation, and success momentum.
  
### 3. Why it is important to "Shine" or Celebrate after each successful completion of habit? (This is the most important concept in today's topic. Whatever you celebrate becomes a habit)
- Celebrating after completing each successful habit is crucial because it creates a positive emotional response, known as **shine** or authentic pride. This feeling reinforces the behavior, building confidence and motivation. Frequent small victories generate success momentum, accelerating habit development and making it more likely for the habit to stick and evolve.
  
### 4. In this [video](https://www.youtube.com/watch?v=mNeXuCYiE0U), what was the most interesting story or idea for you?
- The most interesting idea in this video is the concept of the "aggregation of marginal gains" and the story of Dave Brailsford and British Cycling. The idea that making small, 1% improvements in various aspects of performance can lead to significant success over time is compelling. It shows that minor improvements over time can add to big success. This idea isn't just for sports; it works in many areas of life. It's a helpful concept for improving and reaching your long-term goals.

### 5. What is the book's perspective about Identity?
- The book "Atomic Habits" emphasizes that identity change is the North star of habit change. The key is shifting from outcome-focused goals to adopting habits that align with a desired identity. When a habit becomes part of our identity, it leads to intrinsic motivation and lasting behavioral change.
  
### 6. Write about the book's perspective on how to make a habit easier to do?
- Making the cue obvious by designing your environment to highlight cues for the habit you want to develop, reducing steps between you and positive behaviors.

- Making attractive by making habits appealing, aligning them with the craving aspect of the habit loop.

- Making it easy by minimizing obstacles increases the likelihood of habit formation.

- Making immediate gratification to habits, as our brains prioritize rewards, ensures positive behaviors are reinforced immediately.

### 7. Write about the book's perspective on how to make a habit harder to do?
- Making the cue invisible by designing your environment to reduce exposure to cues triggering the habit.

- Making it unattractive or making it less appealing.

- Making it hard by adding barriers or obstacles.

- Making the reward unsatisfying or discouraging repetition.

### 8. Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?
- I want read more books. So, the steps are:

  - Place a book on the bedside table to be easily seen.
  - Creating a cozy reading setup with comfortable cushions and good lighting.
  - Setting a daily reading goal.
  - Rewarding myself with a small treat after finishing a book.

### 9. Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?
- I want reduce social media usage. So, the steps are:

  - Keeping my phone out of sight.
  - Rearranging app icons hide social media apps in folders.
  - Setting app usage limits on my phone to create a barrier.
  - Reminding myself of the time wasted on social media or the more rewarding activities I could be doing instead.