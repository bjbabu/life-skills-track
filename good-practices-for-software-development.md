# Good Practices for Software Development

___

### 1. Which point(s) were new to you?
- I came to know about new tools like Trello, Jira for documentation.
- Using screenshots, diagrams, screencasts tools like loom to show and explain.
- Look at the way issues get reported in large open-source projects.
- Using the group chat/channels to communicate most of the time. 
- Distraction free focused work.
- Preserving attention is a skill that you can get better with time. 

### 2. Which area do you think you need to improve on? What are your ideas to make progress in that area?
- **Gathering requirements**, From now on, I will write down the requirements.
- **Stuck? Ask questions**, I will ask questions more elaborately and use code snippets.
- **Doing things with 100% involvement**, I will implement a deep work strategy.