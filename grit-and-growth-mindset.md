# Grit and Growth Mindset
___
### 1. Paraphrase (summarize) the [video](https://www.youtube.com/watch?v=H14bBuluwB8) in a few (1 or 2) lines. Use your own words.
- Being successful in school, college and life goes beyond being quick at learning. Significant factors indicating success aren't social intelligence, good looks, physical health, or IQ. It's grit. Grit means having a strong passion and perseverance for long-term goals. It's about having the stamina to stick with your future every day, not just for a week or a month but for years, and putting in a lot of effort to make that future happen. the best idea I've heard about building grit in kids is something called **growth mindset**.

### 2. Paraphrase (summarize) the [video](https://www.youtube.com/watch?v=75GFzikmRY0) in a few (1 or 2) lines in your own words.
- Stanford professor Carol Dweck popularized the concept of growth mindset and uncovered the defining characteristics of the two mindsets, i.e., fixed mindset and growth mindset. In a fixed mindset, people think skills are born, so they can't learn and grow. In a growth mindset, they believe skills are built so they can learn and grow. In a fixed mindset, the focus is on outcomes and insecurity about how they look, while a growth mindset prioritizes the process of learning and getting better. Growth mindset really creates a solid foundation for great learning.

### 3. What is the Internal Locus of Control? What is the key point in the [video](https://www.youtube.com/watch?v=8ZhoeSaPF-k)?
- Internal locus of control refers to an individual's belief that they have control over their life and the outcomes they experience. People with a strong internal locus of control attribute their success or failure to their actions, efforts, and decisions rather than external factors such as luck or fate. Having an internal locus of control is the key to staying motivated.

### 4. What are the key points mentioned by speaker to build growth mindset (explanation not needed).
- Believe in your ability to figure things out.
- Question your assumptions.
- Develop your ywn life curriculum.
- Honor the struggle.
  
### 5. What are your ideas to take action and build Growth Mindset?
- I will stay with a problem till I complete it. I will not quit the problem.
- I know more efforts lead to better understanding.
- I will understand each concept properly.
- I will treat new concepts and situations as learning opportunities. I will never get pressurized by any concept or situation.
