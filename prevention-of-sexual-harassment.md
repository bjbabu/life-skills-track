# Prevention of Sexual Harassment

### 1.  What kinds of behaviour cause sexual harassment?

There are generally three forms of sexual harassment behavior, verbal, visual, and physical.

**Verbal Harassment:**

- Comments about clothing or body.
- Sexual jokes or remarks.
- Requests for sexual favors.
- Repeated asking for dates.
- Sexual innuendos, threats, or rumors.
- Use of foul language.

**Visual Harassment:**

- Sexual content in posters, drawings, or emails.
- Inappropriate screensavers or cartoons.
- Morphing.

**Physical Harassment:**

- Sexual assault.
- Blocking movement.
- Inappropriate touching or gesturing.
- Leering or staring.

There are two categories for each of these types of harassment. They are:


- **Quid Pro Quo:** **"This for that"** Employer/supervisor uses job rewards or punishments to force an employee into a sexual relationship or act.
- **Hostile Work Environment:** Employee's behavior interferes with another's work performance or creates an intimidating/offensive workplace.


### 2.  What would you do in case you face or witness any incident or repeated incidents of such behaviour?

**If i face:**
- First, I record any relevant evidence.
- Address the harasser directly. If it is not safe to do so, then
- Inform supervisors, managers, HR, or whoever is deemed safe.

**If i witness:**
- Offer moral and mental support to the victim.
- Encourage the victim to report the incident to higher authorities.
-Educate them about company policies.
  